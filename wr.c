#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#define FILE_NAME "aaa.txt" // 文件名称

int main()
{
    FILE *fp = NULL; // 文件指针
    char *szAppendStr = "\nText\n";
    int eResult;

    // 以附加方式打开可读/写的文件, 如果没有此文件则会进行创建，然后以附加方式打开可读/写的文件
    fp = fopen(FILE_NAME, "a+");

    // 打开文件失败

    // 将追加内容写入文件指针当前的位置
    fputs(szAppendStr, fp);

    // 最后不要忘了,关闭打开的文件~~~
    fclose(fp);
    return 0;
}