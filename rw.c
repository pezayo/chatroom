
/*
 程序功能：分别按照字符，行标识读取文本文件

*/

// 二、按照行读取文本文件
#include <stdio.h>
#include <stdlib.h>
#define MAX 1024
int fileHang(FILE *fp);

int main(int argc, char *argv[])
{
    char line[MAX];
    int lines = 0;
    char *f1 = argv[1];         //  后台参数
    FILE *fp1 = fopen(f1, "r"); // 创建文件指针及打开文本文件

    if (fp1 == NULL)
    {
        printf("文件 %s 打开时发生错误", f1);
        exit(1);
    }
    fscanf(fp1, "%s", f1);
    printf("f1 is %s\n", f1);

    fileHang(fp1); // 调用自定义函数
    fseek(fp1, 0, SEEK_SET);
    while (1)
    {
        if (fscanf(fp1, "%s", f1) != EOF)
            printf("f1 is %s\n", f1);
        else
            break;
        fgets(f1, 100, fp1);
    }
    fclose(fp1); // 关闭文件指针

    return 0;
}

// 创建子函数
int fileHang(FILE *fp1)
{

    int lines = 0;
    char line[MAX];
    while (fgets(line, MAX, fp1) != NULL)
    {
        lines++;            // 统计行数
        printf("%s", line); // 打印文本
    }
    printf("\n 一共 %d 行", lines);
    return 1;
}
