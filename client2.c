/*socket tcp客户端*/
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "passwdenter.h"

#define SERVER_PORT 5555

int clientSocket;       //客户端socket
char *IP = "127.0.0.1"; //服务器的IP
typedef struct sockaddr SA;
typedef struct userdata
{
    char username[10];
    char password[7];
} Userdata;
char name[10];

//初始化操作，创建socket对象
void init()
{
    clientSocket = socket(PF_INET, SOCK_STREAM, 0);
    struct sockaddr_in addr;
    addr.sin_family = PF_INET;
    addr.sin_port = htons(SERVER_PORT);
    addr.sin_addr.s_addr = inet_addr(IP);
    if (connect(clientSocket, (SA *)&addr, sizeof(addr)) == -1)
    {
        perror("无法连接到服务器");
        exit(-1);
    }
    printf("客户端启动成功\n");
}

//进行登录或者注册操作
void loginregister()
{
    //暂时存储用户输入的用户名和密码
    Userdata *temp = (Userdata *)malloc(sizeof(Userdata));
    //将temp指向的结构存储为buffer字符串
    char *buffer = (char *)malloc(sizeof(Userdata));
    //用于获得服务器的返回状态，判断登录注册是否成功
    int flag;
    //记录是登录还是注册
    char option;
    printf("1. login\n2. register\n");
    printf("Input your option: ");
    //输入1为登录，输入2为注册
    while (1)
    {
        scanf("%c", &option);
        fflush(stdin);
        if (option == '1')
        {
            printf("this is login\n");
            break;
        }
        else if (option == '2')
        {
            printf("this is register\n");
            break;
        }
        else
        {
            //输入不是1，2的数，鉴定为非法字符，重新输入
            printf("the option you input is invalid\nplease input again: ");
            continue;
        }
    }
    //将用户的模式发送给服务器
    send(clientSocket, &option, sizeof(char), 0);
label:
    printf("请输入你的用户名: ");
    scanf("%s", temp->username);
    passwd(temp->password);
    printf("\n");
    //实现temp转换为buffer
    memcpy(buffer, temp, sizeof(Userdata));
    send(clientSocket, buffer, sizeof(Userdata), 0);
    recv(clientSocket, &flag, sizeof(flag), 0); //捕获服务器响应的flag状态
    // printf("the flag is %d\n", flag);
    if (option == '1') //登录状态检测
    {
        switch (flag)
        {
        case 1:
            printf("登录成功！");
            break;
        case 2:
            printf("用户已登录！");
            goto label;
            break;
        case 0:
            printf("用户不存在");
            goto label; //失败，重新尝试
            break;
        case -1:
            printf("密码错误！");
            goto label; //失败，重新尝试
            break;
        default:
            break;
        }
    }
    else //注册状态检测
    {
        switch (flag)
        {
        case 1:
            printf("注册用户成功！");
            break;
        case 0:
            printf("用户已存在，无法注册");
            goto label; //注册失败，重新尝试
            break;
        default:
            break;
        }
    }
    //将最终通过验证的用户名传递给全局变量name
    sprintf(name, "%s", temp->username);
    free(temp);
    free(buffer);
    temp = NULL;
    buffer = NULL;
}

void start()
{
    pthread_t id;
    void *recv_thread(void *);
    pthread_create(&id, 0, recv_thread, 0);
    char buf2[100] = {};
    sprintf(buf2, "%s进入了聊天室", name);
    send(clientSocket, buf2, strlen(buf2), 0);
    while (1)
    {
        char buf[100] = {};
        scanf("%s", buf);
        char msg[131] = {};
        sprintf(msg, "%s:%s", name, buf);
        send(clientSocket, msg, strlen(msg), 0);
        //打印bye退出登录
        if (strcmp(buf, "bye") == 0)
        {
            memset(buf2, 0, sizeof(buf2));
            sprintf(buf2, "%s退出了聊天室", name);
            send(clientSocket, buf2, strlen(buf2), 0);
            break;
        }
    }
    close(clientSocket);
}

//专门用于接受服务器发送的信息的子线程
void *recv_thread(void *p)
{
    while (1)
    {
        char buf[100] = {};
        if (recv(clientSocket, buf, sizeof(buf), 0) <= 0)
        {
            return NULL;
        }
        printf("%s\n", buf);
    }
}

int main()
{
    init();
    // printf("请输入您的名字：");
    // scanf("%s", name);
    loginregister();
    start();
    return 0;
}
