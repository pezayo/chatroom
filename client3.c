/*socket tcp客户端*/
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "passwdenter.h"

#define SERVER_PORT 5555

int clientSocket;       //客户端socket
char *IP = "127.0.0.1"; //服务器的IP
typedef struct sockaddr SA;
typedef struct userdata
{
    char username[10];
    char password[7];
} Userdata;
char name[10];

void filereceive(char *info, int fd);
void loginregister();
void plaintextflietransmission(char *info);

//初始化操作，创建socket对象
void init()
{
    clientSocket = socket(PF_INET, SOCK_STREAM, 0);
    struct sockaddr_in addr;
    addr.sin_family = PF_INET;
    addr.sin_port = htons(SERVER_PORT);
    addr.sin_addr.s_addr = inet_addr(IP);
    if (connect(clientSocket, (SA *)&addr, sizeof(addr)) == -1)
    {
        perror("无法连接到服务器");
        exit(-1);
    }
    printf("客户端启动成功\n");
}

//专门用于接受服务器发送的信息的子线程
void *recv_thread(void *p)
{
    while (1)
    {
        char buf[100] = {};
        if (recv(clientSocket, buf, sizeof(buf), 0) <= 0)
        {
            return NULL;
        }
        printf("%s\n", buf);
        filereceive(buf, clientSocket);
    }
}

//进行登录或者注册操作
void loginregister()
{
    //暂时存储用户输入的用户名和密码
    Userdata *temp = (Userdata *)malloc(sizeof(Userdata));
    //将temp指向的结构存储为buffer字符串
    char *buffer = (char *)malloc(sizeof(Userdata));
    //用于获得服务器的返回状态，判断登录注册是否成功
    int flag;
    //记录是登录还是注册
    char option;
    //输入1为登录，输入2为注册
    while (1)
    {
        printf("1. login\n2. register\n");
        printf("Input your option: ");
        scanf("%c", &option);
        fflush(stdin);
        if (option == '1')
        {
            printf("this is login\n");
            break;
        }
        else if (option == '2')
        {
            printf("this is register\n");
            break;
        }
        else
        {
            //输入不是1，2的数，鉴定为非法字符，重新输入
            printf("the option you input is invalid\nplease input again ");
            continue;
        }
    }
    //将用户的模式发送给服务器
    send(clientSocket, &option, sizeof(char), 0);
label:
    printf("请输入你的用户名: ");
    scanf("%s", temp->username);
    passwd(temp->password);
    printf("\n");
    //实现temp转换为buffer
    memcpy(buffer, temp, sizeof(Userdata));
    send(clientSocket, buffer, sizeof(Userdata), 0);
    recv(clientSocket, (char *)&flag, sizeof(flag), 0); //捕获服务器响应的flag状态
    if (option == '1')                                  //登录状态检测
    {
        switch (flag)
        {
        case 1:
            printf("登录成功！");
            break;
        case 0:
            printf("用户不存在");
            goto label; //失败，重新尝试
            break;
        case -1:
            printf("密码错误！");
            goto label; //失败，重新尝试
            break;
        default:
            break;
        }
    }
    else //注册状态检测
    {
        switch (flag)
        {
        case 1:
            printf("注册用户成功！");
            break;
        case 0:
            printf("用户已存在，无法注册");
            goto label; //注册失败，重新尝试
            break;
        default:
            break;
        }
    }
    sprintf(name, "%s", temp->username);
    free(temp);
    free(buffer);
    temp = NULL;
    buffer = NULL;
}

void filereceive(char *info, int fd)
{
    if (info[0] == '{' && info[strlen(info) - 1] == '}')
    {
        char *filename;
        filename = info;
        filename[strlen(filename) - 1] = '\0';
        filename = filename + 1;
        int r = 1;
        if (r > 0)
        {
            printf("接收到文件名：%s\n", filename);
        }
        int filesize = 0;
        r = recv(fd, (char *)&filesize, 4, 0);
        if (r > 0)
        {
            printf("接收到文件大小：%d\n", filesize);
        }
        FILE *fp = NULL;
        int count = 0;
        char buff[2048];
        fp = fopen(filename, "wb");
        while (1)
        {
            memset(buff, 0, 2048); //清空数组
            r = recv(fd, buff, 2048, 0);
            if (r > 0)
            {
                count += r;
                fwrite(buff, 1, r, fp); //将接收到的数据写入文件
                if (count == filesize)
                {
                    break;
                }
            }
        }
        fflush(fp);
        fclose(fp);
        fp = NULL;
        filename = NULL;
        puts("recv finished");
    }
}

void plaintextflietransmission(char *info)
{
    if (info[0] == '{' && info[strlen(info) - 1] == '}')
    {
        send(clientSocket, info, 100, 0);
        printf("you are sending a file %s", info); // filename
        // char *filename = (char *)malloc(100 * sizeof(char));
        char *filename;
        filename = info;
        filename[strlen(filename) - 1] = '\0';
        filename = filename + 1;
        FILE *fp = fopen(filename, "r");
        int filesize = 0;
        fseek(fp, 0, SEEK_END); //把文件内容指针定位到文件末尾
        filesize = ftell(fp);   //返回文件内容指针到文件头的长度
        fseek(fp, 0, SEEK_SET); //重置文件内容指针，即重新定位到头部
        send(clientSocket, (char *)&filesize, 4, 0);
        char buff[2048];
        int r = 0;
        while (1)
        {
            memset(buff, 0, 2048);
            r = (int)fread(buff, 1, 2048, fp);
            if (r > 0)
            {
                send(clientSocket, buff, r, 0);
            }
            else
            {
                break;
            }
        }
        fflush(fp);
        fclose(fp);
        fp = NULL;
        filename = NULL;
        printf("send finished");
    }
    else
    {
        char msg[131] = {};
        sprintf(msg, "%s:%s", name, info);
        send(clientSocket, msg, strlen(msg), 0);
    }
}

void start()
{
    pthread_t id;
    void *recv_thread(void *);
    pthread_create(&id, 0, recv_thread, 0);
    char buf2[100] = {};
    sprintf(buf2, "%s进入了聊天室", name);
    send(clientSocket, buf2, strlen(buf2), 0);

    while (1)
    {
        char buf[100] = {};
        scanf("%s", buf);
        plaintextflietransmission(buf);
        if (strcmp(buf, "bye") == 0)
        {
            memset(buf2, 0, sizeof(buf2));
            sprintf(buf2, "%s退出了聊天室", name);
            send(clientSocket, buf2, strlen(buf2), 0);
            break;
        }
    }
    close(clientSocket);
}

int main()
{
    init();
    loginregister();
    start();
    return 0;
}
