#include <stdio.h>
#include "conio.h"
void passwd(char *str)
{
    // char str[7] = {};
    int i = 0;
    char userKey;
    printf("请输入6位密码:\n");
    while (1)
    {
        userKey = getch();
        if (userKey == 127)
        {
            putchar('\b');
            putchar(' ');
            putchar('\b');
            i = i > 0 ? i - 1 : 0;
            continue;
        }
        if (i < 6 && userKey != 10)
        {
            printf("*");
            str[i++] = userKey;
        }
        else if (i == 6 && userKey == 10)
        {
            break;
        }
        else
        {
            continue;
        }
    }
    str[i] = '\0';
    printf("\n您输入的是: %s\n", str);
}